cdist-type__p0d_docker(7)
============================================

NAME
----
cdist-type__p0d_docker - Install and configure Docker for my needs.


DESCRIPTION
-----------
Install and configure Docker and (opt-in) tools for my needs,


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------

For each of the following, the version is required as an argument.

dive_
   a tool for exploring each layer in a docker image

dry_
   a Docker manager for the terminal

.. _dive: https://github.com/wagoodman/dive/releases
.. _dry: https://github.com/moncho/dry/releases

BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

   # install everything
    __p0d_docker \
      --dive 0.10.0 \
      --dry 0.11.1


SEE ALSO
--------
:strong:`cdist-type__docker`\ (7),
:strong:`cdist-type__docker_compose`\ (7),
:strong:`cdist-type__docker_config`\ (7),
:strong:`cdist-type__docker_secret`\ (7),
:strong:`cdist-type__docker_stack`\ (7),
:strong:`cdist-type__docker_swarm`\ (7)



AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2021 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
